#!/bin/bash
# Script to setup the usbnet interface to the default IP address
#

sudo modprobe usbnet
sudo ifconfig usb0 192.168.15.201 # configure the usb0 interface to the default IP address
sudo ifconfig usb0 # check the result in ifconfig
