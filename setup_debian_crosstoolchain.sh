#!/bin/sh
# This script sets up armhf cross-compilation on a Debian Jessie system.
# Please note that this installs the cross-toolchain on the main system directly, rather than using a chroot as is recommended (but more complex to setup).
#
# See https://wiki.debian.org/CrossToolchains for more details

curl http://emdebian.org/tools/debian/emdebian-toolchain-archive.key | sudo apt-key add - # add the archive key

echo 'deb http://emdebian.org/tools/debian/ jessie main' | sudo tee -a /etc/apt/sources.list.d/crosstools.list # add the emdebian repo

# install the armhf compiler
sudo dpkg --add-architecture armhf
sudo apt-get update
sudo apt-get install crossbuild-essential-armhf
