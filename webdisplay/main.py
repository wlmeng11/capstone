#!/usr/bin/env python
#
# main.py
#
# This program captures a screenshot of the entire screen and then serves this
# image to a client's browser from memory. No disk access is necessary!

### LIBRARY IMPORTS ###
from __future__ import division  # always use floating point division
import time
import socket
import pyautogui
from flask import Flask, render_template, request

### LOCAL MODULE IMPORTS ###
import webdisplay
import settings
from settings import SETTINGS  # import SETTINGS dictionary into default namespace


def get_network_ip():
    "Return the preferred network IP address"
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # open an internet socket
    s.connect(('8.8.8.8', 0))  # connecting to 8.8.8.8 because it's reliable
    return s.getsockname()[0]


### GLOBAL VARIABLES ###
app = Flask(__name__)
previous_refresh_time = 0.0  # defaults to 0 in Unix time
elapsed_time = 0.0


### ROUTING METHODS ###
@app.route('/')
def index():
    """Render the (static) homepage"""
    return render_template('index.html')


@app.route('/webdisplay', methods=['POST', 'GET'])
def auto():
    """Run web display with the parameters specified by the client"""
    global SETTINGS
    global previous_refresh_time
    global elapsed_time

    while elapsed_time < SETTINGS['refresh_rate']:
        print "Elapsed time %s is less than refresh period %s" % (elapsed_time, SETTINGS['refresh_rate'])
        time.sleep(0.1)
        elapsed_time = time.time() - previous_refresh_time

    previous_refresh_time = time.time()

    if request.method == 'POST':
        try:  # try to parse form data
            settings.parse_form(request)
        except:
            print "Couldn't read form data!"

        try:  # try to emulate a mouse click
            webdisplay.emulate_click(request)
        except:
            print "Couldn't read click data!"

    width, height = SETTINGS['width'], SETTINGS['height']
    #print 'Specified resolution: {0}x{1}\n'.format(width, height)
    return webdisplay.render_screenshot(width, height)


### Program Execution Starts Here! ###
port_num = 5002
try:
    addr = get_network_ip()
except:
    print "Couldn't connect to the Internet! Running server locally instead."
    addr = 'localhost'
print "Connect to this server's IP address: {0}:{1}\n".format(addr, port_num)


app.debug = True  # debug mode automatically reloads server when code is changed
app.run(host='0.0.0.0', port=port_num)  # start the server on the specified port and accept connections from external IP addresses
