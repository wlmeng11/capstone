#!/usr/bin/env python
#
# settings.py
#

from flask import request

### DEFAULT SETTINGS ###
SETTINGS = {
    # dictionary of default settings
    # can be overridden by parse_form()
    'width': 800,
    'height': 600,
    'rotation': 'auto',
    'scaling': 'screen',
    'refresh_rate': 0  # 0 for no refresh, otherwise it's the periodicity of refreshing
}

def parse_form(request):
    """Reads and parses data from the homepage's form, and stores the data in
    the settings """
    resolution = request.form['resolution'].split('x')  # resolution is a list of strings
    SETTINGS['width'] = int(resolution[0])  # width is the first element of resolution, converted to an integer
    SETTINGS['height'] = int(resolution[1])  # height is the second element of resolution, converted to an integer

    SETTINGS['rotation'] = request.form['rotation']
    SETTINGS['scaling'] = request.form['scaling']
    SETTINGS['refresh_rate'] = int(request.form['refresh_rate'])


def print_settings():
    print '\n\nSETTINGS: {0}\n\n'.format(SETTINGS)

# TODO: write a unit test!
