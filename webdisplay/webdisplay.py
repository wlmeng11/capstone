#!/usr/bin/env python
#
# webdisplay.py
#

from __future__ import division  # always use floating point division
import urllib
import pyautogui
import PIL  # https://python-pillow.github.io/
from flask import render_template, request
try:
    # prefer the cStringIO implementation for better performance
    print "Using cStringIO as StringIO!"
    from cStringIO import StringIO
except:
    print "Couldn't import cStringIO! Using StringIO as fallback."
    from StringIO import StringIO


def coordinate_map(clickX, clickY, imageWidth, imageHeight):
    """Map a click coordinate within the specified image resolution to a
    corresponding coordinate on the host's display"""
    screenWidth, screenHeight = pyautogui.size()  # get the host display resolution

    if imageHeight > imageWidth:  # rotate the image for "vertical" devices
        print "Rotating for vertical device!"
        clickX, clickY = (imageHeight - clickY), clickX  # subtract and interchange the image dimensions
        imageWidth, imageHeight = imageHeight, imageWidth  # interchange the image dimensions

    hostX = clickX * screenWidth / imageWidth  # multiply clickX by the horizontal scaling factor
    hostY = clickY * screenHeight / imageHeight  # multiply clickY by the vertical scaling factor
    print "Screen resolution:\t{0}x{1}".format(screenWidth, screenHeight)
    print "Image dimensions:\t{0}x{1}".format(imageWidth, imageHeight)
    print "Host coordinates:\t({0}, {1})".format(hostX, hostY)
    return (hostX, hostY)


# Reference: http://stackoverflow.com/a/10170635
def render_screenshot(width, height):
    """Capture a JPG screenshot, encode it into a base64 StringIO,
    and return a data URL"""
    pil_img = pyautogui.screenshot()  # Note: Linux users must install scrot
    pil_img = pil_img.convert('L')  # convert the image to greyscale
    if height > width:  # rotate the image for "vertical" devices
        pil_img = pil_img.rotate(90, expand=1)

    pil_img.thumbnail((width, height), PIL.Image.ANTIALIAS)  # scale down the image to fit within the specified width and height constraints
    img_io = StringIO()  # initializes a StringIO object to encapsulate the image data
    pil_img.save(img_io, 'JPEG', quality=70)  # write the data from pil_img to img_io acting as a file descriptor
    img_io.seek(0)  # start reading from the beginning of the file
    img_io = img_io.getvalue().encode('base64')  # re-encode the image into base64
    return render_template('display.html', img_data=urllib.quote(img_io.rstrip('\n')))  # return a data URL


def emulate_click(request):
    """Parses the click data from the client and calculates the corresponding host coordinates"""
    clickX = int(request.form['clickX'])
    clickY = int(request.form['clickY'])
    print "Click coordinates:\t({0}, {1})".format(clickX, clickY)
    try:
        imageWidth = int(request.form['imageWidth'])
        imageHeight = int(request.form['imageHeight'])
        print "Image dimensions:\t{0}x{1}".format(imageWidth, imageHeight)
    except:
        print "Couldn't get image dimensions!"

    hostX, hostY = coordinate_map(clickX, clickY, imageWidth, imageHeight)
    pyautogui.click(hostX, hostY)  # emulate a mouse click
