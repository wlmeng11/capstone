# webdisplay #

## Introduction ##
This software is designed to 

## Installation ##
#### System Dependencies ####
On Debian/Ubuntu, run:  
`sudo apt-get install scrot libjpeg-dev python-pip python-Xlib python-tk`  


#### Python Library Dependencies ####
`sudo pip install flask`  
`sudo pip install Pillow`  
`sudo pip install pyautogui`  


### Setup on a Headless Raspberry Pi ###
A virtual framebuffer is needed to run graphical programs on a headless system, so we can use the X virtual framebuffer (xvfb).  
Install it with: `sudo apt-get install xvfb`  

To use it, prefix any command requiring an X11 environment with `xvfb-run`, for example:  
`sudo xvfb-run pip install pyautogui`   
`xvfb-run python webdisplay.py`  


TODO: windows & mac setups!

## Usage ##
Run in a terminal: 
`python webdisplay.py`

Go to a web browser and connect to the specified IP address and port number.  


## References ##
This program was inspired by kranu's eMonitor, which 

And of course, this project would have been much more difficult if it weren't for the efforts of the developers of Python and its libraries that I am using.
